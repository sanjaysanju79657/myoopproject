import java.awt.event.*;
import javax.swing.*;
class Action extends JFrame{
	JButton b1;
	JTextField t1;
	Action(){
		setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("ActionListener");
		t1 = new JTextField();
		t1.setBounds(20,30,120,30);
		add(t1);
		b1 = new JButton("Clear");
		b1.setBounds(40,70,80,30);
		add(b1);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				t1.setText("");
			}
		});
	}
}
public class Asg76 {
	public static void main(String[] args){
		Action a = new Action();
		a.setBounds(300,60,180,160);
		a.setVisible(true);
	}
}